package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.SurveyDateEntity;
import com.terencesweeney.surveyapi.entity.SurveyDatesRepository;
import com.terencesweeney.surveyapi.service.SurveyDateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SurveyDateServiceImpl implements SurveyDateService {

    private final SurveyDatesRepository surveyDatesRepository;

    @Autowired
    public SurveyDateServiceImpl(SurveyDatesRepository surveyDatesRepository) {
        this.surveyDatesRepository = surveyDatesRepository;
    }

    @Override
    public List<SurveyDateEntity> findAll() {
        List<SurveyDateEntity> result = new ArrayList<>();
        this.surveyDatesRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public Optional<SurveyDateEntity> findById(int id) {
        return this.surveyDatesRepository.findById(id);
    }

    @Override
    public SurveyDateEntity createSurveyDate(SurveyDateEntity surveyDateEntity) throws Exception {
        if (surveyDateEntity.getId() != null) {
            throw new IllegalArgumentException("ID should not be specified");
        }

        // if not explicitly provided, set completed to false
        if (surveyDateEntity.getCompleted() == null) {
            surveyDateEntity.setCompleted(false);
        }

        Date currentTs = new Date();
        surveyDateEntity.setCreatedAt(currentTs);
        surveyDateEntity.setUpdatedAt(currentTs);
        return this.surveyDatesRepository.save(surveyDateEntity);
    }

    @Override
    public SurveyDateEntity updateSurveyDate(Integer id, SurveyDateEntity surveyDateEntity) throws Exception {
        if (!id.equals(surveyDateEntity.getId())) {
            throw new IllegalArgumentException("IDs should match");
        }

        return this.surveyDatesRepository.save(surveyDateEntity);
    }
}

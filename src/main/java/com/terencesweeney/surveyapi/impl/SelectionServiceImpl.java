package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.SelectionEntity;
import com.terencesweeney.surveyapi.entity.SelectionsRepository;
import com.terencesweeney.surveyapi.service.SelectionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SelectionServiceImpl implements SelectionService {

    private final SelectionsRepository selectionsRepository;

    @Autowired
    public SelectionServiceImpl(SelectionsRepository selectionsRepository) {
        this.selectionsRepository = selectionsRepository;
    }

    @Override
    public List<SelectionEntity> findAll() {
        ArrayList<SelectionEntity> result = new ArrayList<>();
        this.selectionsRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public SelectionEntity createSelection(SelectionEntity selectionEntity) throws Exception {
        if (selectionEntity.getId() != null) {
            throw new IllegalArgumentException("ID should not be specified");
        }

        Date currentTs = new Date();
        selectionEntity.setCreatedAt(currentTs);
        selectionEntity.setUpdatedAt(currentTs);
        return this.selectionsRepository.save(selectionEntity);
    }

    @Override
    public Optional<SelectionEntity> findById(Integer id) {
        return this.selectionsRepository.findById(id);
    }

    @Override
    public void delete(Integer id) throws Exception {
        SelectionEntity entity = this.selectionsRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Requested Selection was not found."));

        this.selectionsRepository.delete(entity);
    }


    @Override
    public List<SelectionEntity> findByPersonId(Integer personId) {
        if (personId == null) {
            return new ArrayList<>();
        }
        return this.selectionsRepository.findByPersonId(personId);
    }

    @Override
    public SelectionEntity updateSelection(Integer id, SelectionEntity selectionEntity) throws Exception {

        if (!id.equals(selectionEntity.getId())) {
            throw new IllegalArgumentException("IDs should match");
        }

        return saveSelection(id, selectionEntity, true);
    }

    private SelectionEntity saveSelection(Integer id, SelectionEntity selectionEntity, boolean forUpdate)
            throws IllegalArgumentException, EntityNotFoundException {

        if (forUpdate) {
            // update current entity

            // check if an answer with this id exists
            this.selectionsRepository.findById(id)
                    .orElseThrow(() -> new EntityNotFoundException("Requested Answer was not found."));
        }
            return this.selectionsRepository.save(selectionEntity);

      }
    }


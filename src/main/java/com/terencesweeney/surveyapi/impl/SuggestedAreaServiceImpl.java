package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.SuggestedAreaEntity;
import com.terencesweeney.surveyapi.entity.SuggestedAreasRepository;
import com.terencesweeney.surveyapi.entity.SurveyTypeSuggestedAreaEntity;
import com.terencesweeney.surveyapi.entity.SurveyTypeSuggestedAreasRepository;
import com.terencesweeney.surveyapi.service.SuggestedAreaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SuggestedAreaServiceImpl implements SuggestedAreaService {

    private final SuggestedAreasRepository suggestedAreasRepository;
    private final SurveyTypeSuggestedAreasRepository surveyTypeSuggestedAreasRepository;

    @Autowired
    public SuggestedAreaServiceImpl(SuggestedAreasRepository suggestedAreasRepository,
                                    SurveyTypeSuggestedAreasRepository surveyTypeSuggestedAreasRepository) {
        this.suggestedAreasRepository = suggestedAreasRepository;
        this.surveyTypeSuggestedAreasRepository = surveyTypeSuggestedAreasRepository;
    }


    @Override
    public List<SuggestedAreaEntity> findAll() {
        List<SuggestedAreaEntity> result = new ArrayList<>();
        this.suggestedAreasRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public Optional<SuggestedAreaEntity> findById(Integer id) {
        return this.suggestedAreasRepository.findById(id);
    }

    @Override
    public List<SuggestedAreaEntity> findAllForSurveyTypeId(Integer surveyTypeId) throws Exception {

        if (surveyTypeId == null) {
            return new ArrayList<>();
        }

        List<Integer> suggestedAreaIds = this.surveyTypeSuggestedAreasRepository.findBySurveyTypeId(surveyTypeId)
                .stream()
                .map(SurveyTypeSuggestedAreaEntity::getSuggestedAreaId)
                .collect(Collectors.toList());

        return this.suggestedAreasRepository.findAllByIdIn(suggestedAreaIds);
    }

}

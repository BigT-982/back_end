package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.PersonEntity;
import com.terencesweeney.surveyapi.entity.PersonsRepository;
import com.terencesweeney.surveyapi.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PersonServiceImpl implements PersonService {

    private final PersonsRepository personsRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public PersonServiceImpl(PersonsRepository personsRepository,
                             PasswordEncoder passwordEncoder) {
        this.personsRepository = personsRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public List<PersonEntity> findAll() {
        List<PersonEntity> result = new ArrayList<>();
        this.personsRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public Optional<PersonEntity> findById(int id) {
        return this.personsRepository.findById(id);
    }

    @Override
    public Optional<PersonEntity> findByEmail(String id) {
        return this.personsRepository.findByEmail(id);
    }

    @Override
    public PersonEntity createPerson(PersonEntity personEntity) throws Exception {

        if (personEntity.getId() != null) {
            throw new IllegalArgumentException("ID should not be specified");
        }
        return savePerson(null, personEntity, false);
    }

    @Override
    public PersonEntity updatePerson(Integer id, PersonEntity personEntity) throws Exception {

        if (!id.equals(personEntity.getId())) {
            throw new IllegalArgumentException("IDs should match");
        }

        return savePerson(id, personEntity, true);
    }

    @Override
    public PersonEntity authorize(String email, String password) throws Exception {
        Optional<PersonEntity> optionalPersonEntity = this.personsRepository.findFirstByEmail(email);
        if (!optionalPersonEntity.isPresent()) {
            throw new EntityNotFoundException("Person with email " + email + " not found.");
        }

        PersonEntity person = optionalPersonEntity.get();

        String encodedPassword = person.getPassword();
        if (encodedPassword == null) {
            throw new Exception("Password not set for Person with email " + email);
        }

        if (!this.passwordEncoder.matches(password, encodedPassword)) {
            throw new Exception("Username or password invalid");
        }

        return person;
    }

    @Override
    public void delete(Integer id) throws Exception {
        PersonEntity entity = this.personsRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Requested Person was not found."));

        this.personsRepository.delete(entity);
    }

    private PersonEntity savePerson(Integer id, PersonEntity personEntity, boolean forUpdate)
            throws IllegalArgumentException, EntityNotFoundException {

        if (forUpdate) {
            // update current entity

            // check if a person with this id exists
            PersonEntity entity = this.personsRepository.findById(id)
                    .orElseThrow(() -> new EntityNotFoundException("Requested Person was not found."));

            // in case of changing the password, encode the new raw password and set the encoded string as password
            if (personEntity.getPassword() != null) {
                personEntity.setPassword(
                        encodePassword(personEntity.getPassword(), personEntity)
                );
            }
            else {
                personEntity.setPassword(entity.getPassword());
            }


            return this.personsRepository.save(personEntity);
        }

        // create new person

        // check if the email already exists
        if (emailAlreadyExists(personEntity.getEmail())) {
            throw new IllegalArgumentException("Email already exists");
        }

        // save new password if it is included
        if (personEntity.getPassword() != null) {
            personEntity.setPassword(
                    encodePassword(personEntity.getPassword(), personEntity)
            );
        }

        return this.personsRepository.save(personEntity);
    }

    private String encodePassword(String rawPassword, PersonEntity personEntity) {
        return passwordEncoder.encode(rawPassword);
    }

    private boolean emailAlreadyExists(String email) {
        boolean exists = false;
        Optional<PersonEntity> entity = this.personsRepository.findFirstByEmail(email);
        if (entity.isPresent()) {
            exists = true;
        }
        return exists;
    }

}
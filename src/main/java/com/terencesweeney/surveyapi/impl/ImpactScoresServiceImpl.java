package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.ImpactScoresEntity;
import com.terencesweeney.surveyapi.entity.ImpactScoresRepository;
import com.terencesweeney.surveyapi.service.ImpactScoresService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ImpactScoresServiceImpl implements ImpactScoresService {

    private final ImpactScoresRepository impactScoresRepository;

    @Autowired
    public ImpactScoresServiceImpl(ImpactScoresRepository impactScoresRepository) {
        this.impactScoresRepository = impactScoresRepository;
    }

    @Override
    public List<ImpactScoresEntity> findAll() {
        ArrayList<ImpactScoresEntity> result = new ArrayList<>();
        this.impactScoresRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public List<ImpactScoresEntity> findBySurveyDateId(Integer id) {
        if (this.impactScoresRepository.findBySurveyDateId(id).isEmpty()) {
            throw new IllegalArgumentException("Impact Score not available for this ID.");
        }
        return this.impactScoresRepository.findBySurveyDateId(id);
    }
}

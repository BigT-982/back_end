package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.WealthScoresEntity;
import com.terencesweeney.surveyapi.entity.WealthScoresRepository;
import com.terencesweeney.surveyapi.service.WealthScoresService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class WealthScoresServiceImpl implements WealthScoresService {

    private final WealthScoresRepository wealthScoresRepository;

    @Autowired
    public WealthScoresServiceImpl(WealthScoresRepository wealthScoresRepository) {
        this.wealthScoresRepository = wealthScoresRepository;
    }

    @Override
    public List<WealthScoresEntity> findAll() {
        ArrayList<WealthScoresEntity> result = new ArrayList<>();
        this.wealthScoresRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public List<WealthScoresEntity> findBySurveyDateId(Integer id) {
        if (this.wealthScoresRepository.findBySurveyDateId(id).isEmpty()) {
            throw new IllegalArgumentException("Wealth Score not available for this ID.");
        }

        return this.wealthScoresRepository.findBySurveyDateId(id);
    }

    
}

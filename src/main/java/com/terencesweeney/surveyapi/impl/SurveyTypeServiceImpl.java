package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.SurveyTypeEntity;
import com.terencesweeney.surveyapi.entity.SurveyTypesRepository;
import com.terencesweeney.surveyapi.service.SurveyTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SurveyTypeServiceImpl implements SurveyTypeService {

    private final SurveyTypesRepository surveyTypesRepository;

    @Autowired
    public SurveyTypeServiceImpl(SurveyTypesRepository surveyTypesRepository) {
        this.surveyTypesRepository = surveyTypesRepository;
    }


    @Override
    public List<SurveyTypeEntity> findAll() {
        List<SurveyTypeEntity> result = new ArrayList<>();
        this.surveyTypesRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public Optional<SurveyTypeEntity> findById(int id) {
        return this.surveyTypesRepository.findById(id);
    }
}

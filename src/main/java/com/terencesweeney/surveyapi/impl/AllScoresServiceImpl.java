package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.AllScoresEntity;
import com.terencesweeney.surveyapi.entity.AllScoresRepository;
import com.terencesweeney.surveyapi.service.AllScoresService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AllScoresServiceImpl implements AllScoresService {

    private final AllScoresRepository allScoresRepository;

    @Autowired
    public AllScoresServiceImpl(AllScoresRepository allScoresRepository) {
        this.allScoresRepository = allScoresRepository;
    }

    @Override
    public List<AllScoresEntity> findAll() {
        ArrayList<AllScoresEntity> result = new ArrayList<>();
        this.allScoresRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public List<AllScoresEntity> findBySurveyDateId(Integer id) {
        return this.allScoresRepository.findBySurveyDateId(id);
    }
}

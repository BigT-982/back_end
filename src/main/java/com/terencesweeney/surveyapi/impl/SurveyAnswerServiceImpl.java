package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.SurveyAnswerEntity;
import com.terencesweeney.surveyapi.entity.SurveyAnswersRepository;
import com.terencesweeney.surveyapi.service.SurveyAnswerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SurveyAnswerServiceImpl implements SurveyAnswerService {

    private final SurveyAnswersRepository surveyAnswersRepository;

    @Autowired
    public SurveyAnswerServiceImpl(SurveyAnswersRepository surveyAnswersRepository) {
        this.surveyAnswersRepository = surveyAnswersRepository;
    }

    @Override
    public SurveyAnswerEntity answerQuestion(SurveyAnswerEntity surveyAnswer) throws Exception {
        if (surveyAnswer.getId() != null) {
            throw new IllegalArgumentException("ID should not be specified");
        }
        return this.surveyAnswersRepository.save(surveyAnswer);
    }

    @Override
    public List<SurveyAnswerEntity> answerAllQuestions(List<SurveyAnswerEntity> surveyAnswers) throws Exception {
        if (surveyAnswers == null) {
            throw new IllegalArgumentException("No answers included in object");
        }
        ArrayList<SurveyAnswerEntity> result = new ArrayList<>();
        this.surveyAnswersRepository.saveAll(surveyAnswers).forEach(result::add);
        return result;
    }

    @Override
    public List<SurveyAnswerEntity> findAll() {
        ArrayList<SurveyAnswerEntity> result = new ArrayList<>();
        this.surveyAnswersRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public Optional<SurveyAnswerEntity> findById(Integer id) {
        return this.surveyAnswersRepository.findById(id);
    }

    @Override
    public void delete(Integer id) throws Exception {
        SurveyAnswerEntity entity = this.surveyAnswersRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Requested Survey Answer was not found."));

        this.surveyAnswersRepository.delete(entity);
    }

    @Override
    public Optional<SurveyAnswerEntity> findAnswerBySurveyDateIdAndQuestionIdAndSuggestedAreaId(
            Integer surveyDateId, Integer surveyQuestionId, Integer suggestedAreaId
    ) throws IllegalArgumentException {

        if (surveyDateId == null || surveyQuestionId == null || suggestedAreaId == null) {
            throw new IllegalArgumentException("surveyDateId, surveyQuestionId, and suggestedId must not be null");
        }

        return this.surveyAnswersRepository.findFirstBySurveyDateIdAndSurveyQuestionIdAndSuggestedAreaId(
                surveyDateId, surveyQuestionId, suggestedAreaId
        );
    }

    @Override
    public List<SurveyAnswerEntity> findAnswersBySurveyDateId(Integer surveyDateId) {
        if (surveyDateId == null) {
            return new ArrayList<>();
        }
        return this.surveyAnswersRepository.findAllBySurveyDateId(surveyDateId);
    }

    @Override
    public SurveyAnswerEntity updateSurveyAnswer(Integer id, SurveyAnswerEntity surveyAnswerEntity) throws Exception {

        if (!id.equals(surveyAnswerEntity.getId())) {
            throw new IllegalArgumentException("IDs should match");
        }

        return saveSurveyAnswer(id, surveyAnswerEntity, true);
    }

    private SurveyAnswerEntity saveSurveyAnswer(Integer id, SurveyAnswerEntity surveyAnswerEntity, boolean forUpdate)
            throws IllegalArgumentException, EntityNotFoundException {

        if (forUpdate) {
            // update current entity

            // check if an answer with this id exists
            this.surveyAnswersRepository.findById(id)
                    .orElseThrow(() -> new EntityNotFoundException("Requested Answer was not found."));
        }
            return this.surveyAnswersRepository.save(surveyAnswerEntity);

      }
    }


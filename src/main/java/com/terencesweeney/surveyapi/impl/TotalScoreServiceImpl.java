package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.TotalScoreEntity;
import com.terencesweeney.surveyapi.entity.TotalScoreRepository;
import com.terencesweeney.surveyapi.service.TotalScoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TotalScoreServiceImpl implements TotalScoreService {

    private final TotalScoreRepository totalScoreRepository;

    @Autowired
    public TotalScoreServiceImpl(TotalScoreRepository totalScoreRepository) {
        this.totalScoreRepository = totalScoreRepository;
    }

    @Override
    public List<TotalScoreEntity> findAll() {
        ArrayList<TotalScoreEntity> result = new ArrayList<>();
        this.totalScoreRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public Optional<TotalScoreEntity> findById(Integer id) {
        return this.totalScoreRepository.findById(id);
    }

}

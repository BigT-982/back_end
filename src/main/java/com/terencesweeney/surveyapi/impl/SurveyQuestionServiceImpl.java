package com.terencesweeney.surveyapi.impl;

import com.terencesweeney.surveyapi.entity.*;
import com.terencesweeney.surveyapi.service.SuggestedAreaService;
import com.terencesweeney.surveyapi.service.SurveyQuestionService;
import com.terencesweeney.surveyapi.service.SurveyTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SurveyQuestionServiceImpl implements SurveyQuestionService {

    private final SurveyQuestionsRepository surveyQuestionsRepository;
    private final SurveyTypeQuestionsRepository surveyTypeQuestionsRepository;
    private final SurveyAnswersRepository surveyAnswersRepository;
    private final SurveyTypeService surveyTypeService;
    private final SuggestedAreaService suggestedAreaService;


    @Autowired
    public SurveyQuestionServiceImpl(SurveyQuestionsRepository surveyQuestionsRepository,
                                     SurveyTypeQuestionsRepository surveyTypeQuestionsRepository,
                                     SurveyAnswersRepository surveyAnswersRepository,
                                     SurveyTypeService surveyTypeService,
                                     SuggestedAreaService suggestedAreaService) {
        this.surveyQuestionsRepository = surveyQuestionsRepository;
        this.surveyTypeQuestionsRepository = surveyTypeQuestionsRepository;
        this.surveyAnswersRepository = surveyAnswersRepository;
        this.surveyTypeService = surveyTypeService;
        this.suggestedAreaService = suggestedAreaService;
    }

    @Override
    public List<SurveyQuestionEntity> findAll() {
        List<SurveyQuestionEntity> result = new ArrayList<>();
        this.surveyQuestionsRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public Optional<SurveyQuestionEntity> findById(Integer id) {
        return this.surveyQuestionsRepository.findById(id);
    }



    @Override
    public List<SurveyQuestionEntity> findAllForSurveyTypeId(Integer surveyTypeId) throws Exception {

        if (surveyTypeId == null) {
            return new ArrayList<>();
        }

        List<Integer> surveyQuestionIds = this.surveyTypeQuestionsRepository.findBySurveyTypeId(surveyTypeId)
                .stream()
                .map(SurveyTypeQuestionEntity::getSurveyQuestionId)
                .collect(Collectors.toList());

        return this.surveyQuestionsRepository.findAllByIdIn(surveyQuestionIds);
    }

}

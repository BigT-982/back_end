package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.TotalScoreEntity;

import java.util.List;
import java.util.Optional;

public interface TotalScoreService {
    List<TotalScoreEntity> findAll();
    Optional<TotalScoreEntity> findById(Integer id);
}



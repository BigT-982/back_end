package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.AllScoresEntity;

import java.util.List;

public interface AllScoresService {
    List<AllScoresEntity> findAll();
    List<AllScoresEntity>findBySurveyDateId(Integer id) throws Exception;
}



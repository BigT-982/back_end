package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.SurveyTypeEntity;

import java.util.List;
import java.util.Optional;

public interface SurveyTypeService {
    List<SurveyTypeEntity> findAll();
    Optional<SurveyTypeEntity> findById(int id);
}

package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.PersonEntity;

import java.util.List;
import java.util.Optional;

public interface PersonService {
    List<PersonEntity> findAll();
    Optional<PersonEntity> findById(int id);
    PersonEntity createPerson(PersonEntity personEntity) throws Exception;
    PersonEntity updatePerson(Integer id, PersonEntity personEntity) throws Exception;
    PersonEntity authorize(String email, String password) throws Exception;
    void delete(Integer id) throws Exception;

    Optional<PersonEntity> findByEmail(String id);
}

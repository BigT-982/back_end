package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.SuggestedAreaEntity;

import java.util.List;
import java.util.Optional;

public interface SuggestedAreaService {
    List<SuggestedAreaEntity> findAllForSurveyTypeId(Integer surveyTypeId) throws Exception;
    List<SuggestedAreaEntity> findAll();
    Optional<SuggestedAreaEntity> findById(Integer id);
}

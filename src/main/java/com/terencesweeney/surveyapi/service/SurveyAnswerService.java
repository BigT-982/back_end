package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.SurveyAnswerEntity;

import java.util.List;
import java.util.Optional;

public interface SurveyAnswerService {
    List<SurveyAnswerEntity> findAll();
    Optional<SurveyAnswerEntity> findById(Integer id);
    SurveyAnswerEntity updateSurveyAnswer(Integer id, SurveyAnswerEntity surveyAnswerEntity) throws Exception;
    void delete(Integer id) throws Exception;
    Optional<SurveyAnswerEntity> findAnswerBySurveyDateIdAndQuestionIdAndSuggestedAreaId(
            Integer surveyDateId, Integer surveyQuestionId, Integer suggestedAreaId
    ) throws IllegalArgumentException;
    List<SurveyAnswerEntity> findAnswersBySurveyDateId(Integer surveyDateId);
    SurveyAnswerEntity answerQuestion(SurveyAnswerEntity surveyAnswer) throws Exception;
    List<SurveyAnswerEntity> answerAllQuestions(List<SurveyAnswerEntity> surveyAnswers) throws Exception;

}

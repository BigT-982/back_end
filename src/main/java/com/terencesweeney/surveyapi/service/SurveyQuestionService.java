package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.SurveyAnswerEntity;
import com.terencesweeney.surveyapi.entity.SurveyQuestionEntity;

import java.util.List;
import java.util.Optional;

public interface SurveyQuestionService {
    List<SurveyQuestionEntity> findAllForSurveyTypeId(Integer surveyTypeId) throws Exception;
    List<SurveyQuestionEntity> findAll();
    Optional<SurveyQuestionEntity> findById(Integer id);
}

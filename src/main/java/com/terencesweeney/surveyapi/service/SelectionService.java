package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.SelectionEntity;

import java.util.List;
import java.util.Optional;

public interface SelectionService {
    List<SelectionEntity> findAll();
    Optional<SelectionEntity> findById(Integer id);
    SelectionEntity createSelection(SelectionEntity selectionEntity) throws Exception;
    SelectionEntity updateSelection(Integer id, SelectionEntity selectionsEntity) throws Exception;
    void delete(Integer id) throws Exception;
    List<SelectionEntity> findByPersonId(Integer personId);
}

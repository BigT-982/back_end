package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.WealthScoresEntity;

import java.util.List;

public interface WealthScoresService {
    List<WealthScoresEntity> findAll();
    List<WealthScoresEntity>findBySurveyDateId(Integer id) throws Exception;
}



package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.SurveyDateEntity;

import java.util.List;
import java.util.Optional;

public interface SurveyDateService {
    List<SurveyDateEntity> findAll();
    Optional<SurveyDateEntity> findById(int id);
    SurveyDateEntity createSurveyDate(SurveyDateEntity surveyDateEntity) throws Exception;
    SurveyDateEntity updateSurveyDate(Integer id, SurveyDateEntity surveyDateEntity) throws Exception;
}

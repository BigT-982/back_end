package com.terencesweeney.surveyapi.service;

import com.terencesweeney.surveyapi.entity.ImpactScoresEntity;

import java.util.List;

public interface ImpactScoresService {
    List<ImpactScoresEntity> findAll();
    List<ImpactScoresEntity>findBySurveyDateId(Integer id) throws Exception;
}



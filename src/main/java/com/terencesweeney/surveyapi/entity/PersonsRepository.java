package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonsRepository extends CrudRepository<PersonEntity, Integer> {
    Optional<PersonEntity> findFirstByEmail(final String email);
    Optional<PersonEntity> findByEmail(final String email);
}

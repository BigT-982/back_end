package com.terencesweeney.surveyapi.entity;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = false)

public class SurveyDateAreaPK implements Serializable {
    private Integer surveyDateId;

    private String actualArea;

    // default constructor
    public SurveyDateAreaPK(){}

    public SurveyDateAreaPK(Integer surveyDateId, String actualArea) {
        this.surveyDateId = surveyDateId;
        this.actualArea = actualArea;
    }

}
package com.terencesweeney.surveyapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = false)
@IdClass(SurveyDateAreaPK.class)
@Entity
@Table(name = "impact_scores")
@JsonIgnoreProperties(ignoreUnknown = true)

public class ImpactScoresEntity {

    @Id private Integer surveyDateId;

    @Id private String actualArea;

    @Column(name = "wealth")
    private double wealth;

    @Column(name = "impact")
    private double impact;

}

package com.terencesweeney.surveyapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "survey_types_questions")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SurveyTypeQuestionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "survey_type_id")
    private Integer surveyTypeId;

    @Column(name = "survey_question_id")
    private Integer surveyQuestionId;

    @Column(name = "question_number")
    private Integer questionNumber;

}

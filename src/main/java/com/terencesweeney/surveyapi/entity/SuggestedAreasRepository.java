package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SuggestedAreasRepository extends CrudRepository<SuggestedAreaEntity, Integer> {
    List<SuggestedAreaEntity> findAllByIdIn(final List<Integer> ids);
}

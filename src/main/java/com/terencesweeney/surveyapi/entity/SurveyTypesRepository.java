package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SurveyTypesRepository extends CrudRepository<SurveyTypeEntity, Integer> {
}

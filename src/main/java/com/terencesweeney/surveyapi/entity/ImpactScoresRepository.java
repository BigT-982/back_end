package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImpactScoresRepository extends CrudRepository<ImpactScoresEntity, Integer> {
    List<ImpactScoresEntity> findBySurveyDateId(Integer id);
}

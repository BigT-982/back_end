package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SurveyAnswersRepository extends CrudRepository<SurveyAnswerEntity, Integer> {
    List<SurveyAnswerEntity> findAllBySurveyDateId(final Integer surveyDateId);
    Optional<SurveyAnswerEntity> findFirstBySurveyDateIdAndSurveyQuestionIdAndSuggestedAreaId(
            final Integer surveyDateId, final Integer surveyQuestionId, final Integer suggestedAreaId
    );
}

package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyQuestionsRepository extends CrudRepository<SurveyQuestionEntity, Integer> {
    List<SurveyQuestionEntity> findAllByIdIn(final List<Integer> ids);
}

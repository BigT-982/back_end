package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TotalScoreRepository extends CrudRepository<TotalScoreEntity, Integer> {

}

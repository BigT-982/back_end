package com.terencesweeney.surveyapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "survey_answers")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SurveyAnswerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "survey_date_id")
    private Integer surveyDateId;

    @Column(name = "survey_question_id")
    private Integer surveyQuestionId;

    @Column(name = "actual_area")
    private String actualArea;

    @Column(name = "answer")
    private Integer answer;

    @Column(name = "suggested_area_id")
    private Integer suggestedAreaId;

}

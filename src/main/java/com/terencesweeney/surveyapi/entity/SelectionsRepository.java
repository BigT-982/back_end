package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SelectionsRepository extends CrudRepository<SelectionEntity, Integer> {
    List<SelectionEntity> findByPersonId(final Integer surveyDateId);
}

package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WealthScoresRepository extends CrudRepository<WealthScoresEntity, Integer> {
    List<WealthScoresEntity> findBySurveyDateId(Integer id);
}

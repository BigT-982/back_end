package com.terencesweeney.surveyapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "survey_dates")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SurveyDateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "person_id")
    private Integer personId;

    @Column(name = "survey_type_id")
    private Integer surveyTypeId;

    @Column(name = "is_completed", nullable = false, columnDefinition = "TINYINT", length = 1)
    @JsonProperty(defaultValue = "false")
    private Boolean completed;

    @Column(name = "created_at")
    @Temporal(TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(name = "updated_at")
    @Temporal(TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    @Column(name = "survey_group_id")
    private String surveyGroupId;
}

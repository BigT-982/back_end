package com.terencesweeney.surveyapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = false)
@IdClass(SurveyDateAreaPK.class)
@Entity
@Table(name = "all_scores")
@JsonIgnoreProperties(ignoreUnknown = true)

public class AllScoresEntity {

    @Id private Integer surveyDateId;

    @Id private String actualArea;

    @Column(name = "wealth")
    private double wealth;

    @Column(name = "ideal_wealth")
    private double idealWealth;

    @Column(name = "impact")
    private double impact;

    @Column(name = "area_score")
    private double areaScore;

}

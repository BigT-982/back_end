package com.terencesweeney.surveyapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "selections")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SelectionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "person_id")
    private Integer personId;

    @Column(name = "survey_date_id")
    private Integer surveyDateId;

    @Column(name = "actual_area")
    private String actualArea;

    @Column(name = "ranking")
    private Integer ranking;

    @Column(name = "created_at")
    @Temporal(TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(name = "updated_at")
    @Temporal(TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

}

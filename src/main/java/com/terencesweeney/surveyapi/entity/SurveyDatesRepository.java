package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SurveyDatesRepository extends CrudRepository<SurveyDateEntity, Integer> {
}

package com.terencesweeney.surveyapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.sql.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "survey_types")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SurveyTypeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "survey_number")
    private Integer surveyNumber;

    @Column(name = "description")
    private String description;

    @Column(name = "initial_message")
    private String initialMessage;

    @Column(name = "input_email_message")
    private String inputEmailMessage;

    @Column(name = "login")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean login;

    @Column(name = "password_message")
    private String passwordMessage;

    @Column(name = "instructions")
    private String instructions;

    @Column(name = "final_message")
    private String finalMessage;

    @Column(name = "final_button_text")
    private String finalButtonText;

    @Column(name = "display_results")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean displayResults;

    @Column(name = "display_wealth_scores")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean displayWealthScores;

    @Column(name = "display_impact_scores")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean displayImpactScores;

    @Column(name = "display_total_score")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean displayTotalScore;

    @Column(name = "display_areas")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean displayAreas;

    @Column(name = "select_areas")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean selectAreas;

    @Column(name = "email_participant")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean emailParticipant;

    @Column(name = "num_areas_to_display")
    private Integer numAreasToDisplay;

    @Column(name = "email_template_id")
    private Integer emailTemplateId;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_Date")
    private Date endDate;

    @Column(name = "is_open")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean isOpen;

    @Column(name = "main_logo_url")
    private String mainLogoURL;

    @Column(name = "background_image_url")
    private String backgroundImageURL;

    @Column(name = "final_image_url")
    private String finalImageURL;

    @Column(name = "redirect_url")
    private String redirectURL;

    @Column(name = "is_partnered")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean isPartnered;

    @Column(name = "partnered_message")
    private String partneredMessage;
}

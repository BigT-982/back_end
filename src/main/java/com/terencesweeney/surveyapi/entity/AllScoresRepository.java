package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AllScoresRepository extends CrudRepository<AllScoresEntity, Integer> {
    List<AllScoresEntity> findBySurveyDateId(Integer id);
}

package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyTypeQuestionsRepository extends CrudRepository<SurveyTypeQuestionEntity, Integer> {
    List<SurveyTypeQuestionEntity> findBySurveyTypeId(final Integer surveyTypeId);
}

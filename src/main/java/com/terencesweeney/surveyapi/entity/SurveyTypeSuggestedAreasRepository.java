package com.terencesweeney.surveyapi.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyTypeSuggestedAreasRepository extends CrudRepository<SurveyTypeSuggestedAreaEntity, Integer> {
    List<SurveyTypeSuggestedAreaEntity> findBySurveyTypeId(Integer surveyTypeId);
}

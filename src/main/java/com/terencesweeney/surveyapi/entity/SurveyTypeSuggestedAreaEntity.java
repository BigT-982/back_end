package com.terencesweeney.surveyapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "survey_types_suggested_areas")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SurveyTypeSuggestedAreaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "survey_type_id")
    private Integer surveyTypeId;

    @Column(name = "suggested_area_id")
    private Integer suggestedAreaId;

}

package com.terencesweeney.surveyapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "survey_questions")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SurveyQuestionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "group_name")
    private String groupName;

    @Column(name = "question_pt1")
    private String questionPt1;

    @Column(name = "question_pt2")
    private String questionPt2;

    @Column(name = "question_pt3")
    private String questionPt3;

    @Column(name = "lower_label")
    private String lowerLabel;

    @Column(name = "upper_label")
    private String upperLabel;

    @Column(name = "question_type")
    private Integer questionType;
}

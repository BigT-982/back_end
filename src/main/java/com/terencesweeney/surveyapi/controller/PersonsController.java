package com.terencesweeney.surveyapi.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.terencesweeney.surveyapi.entity.PersonEntity;
import com.terencesweeney.surveyapi.http.ErrorResponse;
import com.terencesweeney.surveyapi.http.NotFoundResponse;
import com.terencesweeney.surveyapi.http.PersonAuthRequest;
import com.terencesweeney.surveyapi.http.SuccessResponse;
import com.terencesweeney.surveyapi.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/persons")
@PreAuthorize("hasRole('ADMIN')")
public class PersonsController {

    private final PersonService personService;

    @Autowired
    public PersonsController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public List<PersonEntity> index() {
        return this.personService.findAll();
    }



    @GetMapping("/{id}")
    public PersonEntity show(@PathVariable Integer id) {
        return this.personService.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Requested Person was not found.")
        );
    }

    @PostMapping("/email")
    public PersonEntity findByEmail(@RequestBody PersonEntity personEntity) throws Exception {
        return personService.findByEmail(personEntity.getEmail()).orElseThrow(
                () -> new EntityNotFoundException("Requested Person was not found."));
    }

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public PersonEntity create(@RequestBody PersonEntity personEntity) throws Exception {
        return personService.createPerson(personEntity);
    }

    @PatchMapping("/{id}")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public PersonEntity update(@PathVariable(name = "id") Integer id, @RequestBody PersonEntity personEntity) throws Exception {
        return personService.updatePerson(id, personEntity);
    }

    @DeleteMapping("/{id}")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> delete(@PathVariable(name = "id") Integer personId) throws Exception {
        personService.delete(personId);
        return ResponseEntity
                .accepted()
                .body(SuccessResponse.of(personId, "Person was successfully deleted."));
    }

    @PostMapping("/auth")
    public PersonEntity authorize(@RequestBody @Validated PersonAuthRequest request) throws Exception {
        return personService.authorize(request.getEmail(), request.getPassword());
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<?> handleErrors(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new NotFoundResponse(ex.getMessage()));
    }

    @ExceptionHandler({InvalidFormatException.class, IllegalArgumentException.class})
    public ResponseEntity<?> handleValidationErrors(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage());
        return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handleExceptionErrors(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage())
        );
    }
}

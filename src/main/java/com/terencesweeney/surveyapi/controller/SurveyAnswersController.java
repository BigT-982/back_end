package com.terencesweeney.surveyapi.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.terencesweeney.surveyapi.entity.SurveyAnswerEntity;
import com.terencesweeney.surveyapi.http.ErrorResponse;
import com.terencesweeney.surveyapi.http.FindSurveyAnswersRequest;
import com.terencesweeney.surveyapi.http.NotFoundResponse;
import com.terencesweeney.surveyapi.http.SuccessResponse;
import com.terencesweeney.surveyapi.service.SurveyAnswerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/answers")
@PreAuthorize("hasRole('ADMIN')")
public class SurveyAnswersController {

    private final SurveyAnswerService surveyAnswerService;

    @Autowired
    public SurveyAnswersController(SurveyAnswerService surveyAnswerService) {
        this.surveyAnswerService = surveyAnswerService;
    }

    @GetMapping
    public List<SurveyAnswerEntity> index() {
        return this.surveyAnswerService.findAll();
    }

    @GetMapping("/{id}")
    public SurveyAnswerEntity show(@PathVariable Integer id) {
        return this.surveyAnswerService.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Requested Survey Question was not found.")
        );
    }

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public SurveyAnswerEntity answer(@RequestBody SurveyAnswerEntity surveyAnswer) throws Exception {
        return this.surveyAnswerService.answerQuestion(surveyAnswer);
    }


    @PostMapping("/all")
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public List<SurveyAnswerEntity> answerAll(@RequestBody List<SurveyAnswerEntity> surveyAnswers) throws Exception {
        return this.surveyAnswerService.answerAllQuestions(surveyAnswers);
    }

    @DeleteMapping("/{id}")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> delete(@PathVariable(name = "id") Integer surveyAnswerId) throws Exception {
        this.surveyAnswerService.delete(surveyAnswerId);
        return ResponseEntity
                .ok()
                .body(SuccessResponse.of(surveyAnswerId, "Survey Answer was successfully deleted."));
    }

    @PostMapping("/findone")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public SurveyAnswerEntity findAnswerBySurveyDateIdAndSurveyQuestionId(@RequestBody FindSurveyAnswersRequest r) throws Exception {
        return this.surveyAnswerService.findAnswerBySurveyDateIdAndQuestionIdAndSuggestedAreaId(
                r.getSurveyDateId(), r.getSurveyQuestionId(), r.getSuggestedAreaId()
        ).orElseThrow(
                () -> new EntityNotFoundException(
                        "No Survey Answer found for survey_date_id " + r.getSurveyDateId()
                                + " and survey_question_id " + r.getSurveyQuestionId()
                                + " and suggested_area_id " + r.getSuggestedAreaId()
                )
        );
    }

    @PostMapping("/find")
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public List<SurveyAnswerEntity> findAnswerBySurveyDateId(@RequestBody FindSurveyAnswersRequest r) throws Exception {
        return this.surveyAnswerService.findAnswersBySurveyDateId(r.getSurveyDateId());
    }


    @PatchMapping("/{id}")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public SurveyAnswerEntity update(@PathVariable(name = "id") Integer id, @RequestBody SurveyAnswerEntity surveyAnswerEntity) throws Exception {
        return surveyAnswerService.updateSurveyAnswer(id, surveyAnswerEntity);
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<?> handleErrors(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new NotFoundResponse(ex.getMessage()));
    }

    @ExceptionHandler({InvalidFormatException.class, IllegalArgumentException.class})
    public ResponseEntity<?> handleValidationErrors(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage());
        return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handleExceptionErrors(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage())
        );
    }
}

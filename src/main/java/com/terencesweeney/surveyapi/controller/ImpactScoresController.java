package com.terencesweeney.surveyapi.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.terencesweeney.surveyapi.entity.ImpactScoresEntity;
import com.terencesweeney.surveyapi.http.ErrorResponse;
import com.terencesweeney.surveyapi.http.NotFoundResponse;
import com.terencesweeney.surveyapi.service.ImpactScoresService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/impactScores")
@PreAuthorize("hasRole('ADMIN')")
public class ImpactScoresController {

    private final ImpactScoresService impactScoresService;

    @Autowired
    public ImpactScoresController(ImpactScoresService impactScoresService) {
        this.impactScoresService = impactScoresService;
    }

    @GetMapping
    public List<ImpactScoresEntity> index() {
        return this.impactScoresService.findAll();
    }

    @GetMapping("/surveyDate/{id}")
    public List<ImpactScoresEntity> index(@PathVariable Integer id) throws Exception {
        return this.impactScoresService.findBySurveyDateId(id);
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<?> handleErrors(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new NotFoundResponse(ex.getMessage()));
    }

    @ExceptionHandler({InvalidFormatException.class, IllegalArgumentException.class})
    public ResponseEntity<?> handleValidationErrors(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage());
        return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handleExceptionErrors(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage())
        );
    }
}


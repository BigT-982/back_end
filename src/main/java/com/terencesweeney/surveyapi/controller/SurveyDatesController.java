package com.terencesweeney.surveyapi.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.terencesweeney.surveyapi.entity.SurveyDateEntity;
import com.terencesweeney.surveyapi.http.ErrorResponse;
import com.terencesweeney.surveyapi.http.NotFoundResponse;
import com.terencesweeney.surveyapi.service.SurveyDateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/surveydates")
@PreAuthorize("hasRole('ADMIN')")
public class SurveyDatesController {

    private final SurveyDateService surveyDateService;

    @Autowired
    public SurveyDatesController(SurveyDateService surveyDateService) {
        this.surveyDateService = surveyDateService;
    }

    @GetMapping
    public ResponseEntity<List<SurveyDateEntity>> index() {
        return ResponseEntity.ok(this.surveyDateService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> show(@PathVariable Integer id) {
        return ResponseEntity.ok(
                this.surveyDateService.findById(id).orElseThrow(
                        () -> new EntityNotFoundException("Requested SurveyDate was not found.")
                )
        );
    }

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public SurveyDateEntity create(@RequestBody SurveyDateEntity surveyDateEntity) throws Exception {
        return surveyDateService.createSurveyDate(surveyDateEntity);
    }

    @PatchMapping("/{id}")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public SurveyDateEntity update(@PathVariable(name = "id") Integer id, @RequestBody SurveyDateEntity surveyDateEntity) throws Exception {
        return surveyDateService.updateSurveyDate(id, surveyDateEntity);
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<?> handleErrors(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new NotFoundResponse(ex.getMessage()));
    }

    @ExceptionHandler({InvalidFormatException.class, IllegalArgumentException.class})
    public ResponseEntity<?> handleValidationErrors(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage());
        return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handleExceptionErrors(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage())
        );
    }
}

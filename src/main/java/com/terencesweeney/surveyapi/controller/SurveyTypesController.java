package com.terencesweeney.surveyapi.controller;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.terencesweeney.surveyapi.entity.SuggestedAreaEntity;
import com.terencesweeney.surveyapi.entity.SurveyQuestionEntity;
import com.terencesweeney.surveyapi.entity.SurveyTypeEntity;
import com.terencesweeney.surveyapi.http.ErrorResponse;
import com.terencesweeney.surveyapi.http.NotFoundResponse;
import com.terencesweeney.surveyapi.service.SuggestedAreaService;
import com.terencesweeney.surveyapi.service.SurveyQuestionService;
import com.terencesweeney.surveyapi.service.SurveyTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/surveytypes")
@PreAuthorize("hasRole('ADMIN')")
public class SurveyTypesController {

    private final SurveyTypeService surveyTypeService;
    private final SurveyQuestionService surveyQuestionService;
    private final SuggestedAreaService suggestedAreaService;

    @Autowired
    public SurveyTypesController(SurveyTypeService surveyTypeService, SurveyQuestionService surveyQuestionService,
                                 SuggestedAreaService suggestedAreaService) {
        this.surveyTypeService = surveyTypeService;
        this.surveyQuestionService = surveyQuestionService;
        this.suggestedAreaService = suggestedAreaService;
    }

    @GetMapping
    public ResponseEntity<List<SurveyTypeEntity>> index() {
        return ResponseEntity.ok(this.surveyTypeService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> show(@PathVariable Integer id) {
        return ResponseEntity.ok(
                this.surveyTypeService.findById(id).orElseThrow(
                        () -> new EntityNotFoundException("Requested SurveyType was not found.")
                )
        );
    }

    @GetMapping("/{id}/questions")
    public List<SurveyQuestionEntity> findQuestions(@PathVariable Integer id) throws Exception {
        return this.surveyQuestionService.findAllForSurveyTypeId(id);
    }

    @GetMapping("/{id}/areas")
    public List<SuggestedAreaEntity> findSuggestedAreas(@PathVariable Integer id) throws Exception {
        return this.suggestedAreaService.findAllForSurveyTypeId(id);
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<?> handleErrors(HttpServletRequest req, Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new NotFoundResponse(ex.getMessage()));
    }

    @ExceptionHandler({InvalidFormatException.class, IllegalArgumentException.class})
    public ResponseEntity<?> handleValidationErrors(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage());
        return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.BAD_REQUEST.value(), ex.getMessage()));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handleExceptionErrors(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage())
        );
    }
}

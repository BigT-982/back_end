package com.terencesweeney.surveyapi.http;

import lombok.Data;

import java.util.Date;

@Data
public class SuccessResponse {
    private final Integer id;
    private final String message;
    private final Date timestamp;

    protected SuccessResponse(final Integer id, final String message) {
        this.id = id;
        this.message = message;
        this.timestamp = new Date();
    }

    public static SuccessResponse of(final Integer id, final String message) {
        return new SuccessResponse(id, message);
    }
}

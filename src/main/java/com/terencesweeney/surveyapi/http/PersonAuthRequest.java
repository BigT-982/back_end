package com.terencesweeney.surveyapi.http;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PersonAuthRequest {

    private final String email;

    private final String password;

    @JsonCreator
    public PersonAuthRequest(@JsonProperty(value = "email", required = true) String email,
                             @JsonProperty(value = "password", required = true) String password) {
        this.email = email;
        this.password = password;
    }
}

package com.terencesweeney.surveyapi.http;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

@Data
public class ErrorResponse {

    private final Date timestamp;
    private final Map<?,?> error;

    protected ErrorResponse(int code, String message) {
        this.timestamp = new Date();

        Map<String, Object> errorMap = new TreeMap<>();
        errorMap.put("code", code);
        errorMap.put("message", message);
        this.error = errorMap;
    }

    public static ErrorResponse of(String message) {
        return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
    }

    public static ErrorResponse of(int code, String message) {
        return new ErrorResponse(code, message);
    }
}

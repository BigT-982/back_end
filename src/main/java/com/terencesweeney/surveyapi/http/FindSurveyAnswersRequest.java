package com.terencesweeney.surveyapi.http;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FindSurveyAnswersRequest {

    private final Integer surveyDateId;

    private final Integer surveyQuestionId;

    private final Integer suggestedAreaId;

    @JsonCreator
    public FindSurveyAnswersRequest(
            @JsonProperty(value = "surveyDateId", required = true) Integer surveyDateId,
            @JsonProperty(value = "suggestedAreaId") Integer suggestedAreaId,
            @JsonProperty(value = "surveyQuestionId") Integer surveyQuestionId
    ) {
        this.surveyDateId = surveyDateId;
        this.suggestedAreaId = suggestedAreaId;
        this.surveyQuestionId = surveyQuestionId;
    }
}

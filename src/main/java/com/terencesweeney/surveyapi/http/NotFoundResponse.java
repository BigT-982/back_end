package com.terencesweeney.surveyapi.http;

import lombok.Data;

import java.util.Date;

@Data
public class NotFoundResponse {

    private final String message;
    private final Date timestamp;

    public NotFoundResponse() {
        this("The requested resource was not found", new Date());
    }

    public NotFoundResponse(String message) {
        this(message, new Date());
    }

    private NotFoundResponse(String message, Date timestamp) {
        this.message = message;
        this.timestamp = timestamp;
    }
}
